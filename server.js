const net = require('net')
const ws = require('ws')
const fs = require('fs')
const express = require('express')
const http = require('http')

var activeConnections = 0,
    totalConnections = 0

// Config
const HTTPport = 25891
const WSport = 27561
//const ssl_key = 'webterm_retrohackers_com.key'
//const ssl_cert = 'webterm_retrohackers_com.cert'

// Keys for SSL
//const privateKey  = fs.readFileSync(ssl_key, 'utf8')
//const certificate = fs.readFileSync(ssl_cert, 'utf8')

//if (!privateKey || !certificate) console.error('Error: Missing key or cert file')

// Create HTTPS server to serve SSL for WSS
//const credentials = {key: privateKey, cert: certificate}

const app = express()
const httpServer = http.createServer(app)
httpServer.listen(HTTPport)

// Serve static files
const staticPath = (process.env.DIST || '.') + '/browser'
app.use(express.static(staticPath))

// Create WS server
const wss = new ws.Server({
    port: WSport
})

wss.on('connection', (ws) => {
    var client

    console.log('o Incoming websocket connection', ws.upgradeReq.connection.remoteAddress)
    totalConnections++
    activeConnections++
    logStats()

    function sendWS(data) {
        if (ws.readyState === ws.OPEN)
            ws.send(data)
        else
            console.warn('! websocket not open for sending')
    }

    ws.on('message', (message) => {
        // console.log('received: %s', message)

        if (message == '+++ATZ') {
            console.log('o Received ATZ')
            sendWS('ATZ\r\nOK\r\n')
            return
        }

        if (message.indexOf('+++DIAL ') == 0) {
            const substr = message.slice(8)
            const host = substr.split(':')
            // if (client && (client.remoteAddress || client.connecting)) {
            if (client) {
                console.log('o closing BBS connection')
                client.destroy()
                // client.on('data', function () {})
                // client.on('error', function () {})
                // client.on('close', function () {})
                client = null
            }
            client = connectToBBS(host[0], host[1], ws, (evt) => {
                if (evt == 'close')
                    client = null;
            })
            return
        }

        if (message == '+++HANGUP') {
            console.log('o Received HANGUP request')
            if (client)
                // client.end()
                client.destroy()
            return
        }

        if (client && client.remoteAddress) {
            try {
                client.write(new Buffer([message])) //send to bbs as UInt8
            } catch (error) {
                console.error(error);
            }
        } else {
            sendWS(new Buffer([message]));
        }
    })

    ws.on('close', (code, reason) => {
        console.warn('WS closed ', code, reason)
        activeConnections--
        logStats()
        if (client && client.remoteAddress)
            client.end()
    })

    ws.on('error', (error) => {
        console.warn('WS error', error)
    })
})

function connectToBBS(host, port, ws, callback) {
    console.log('dialing ', host, port)

    function sendWS(data) {
        if (ws.readyState === ws.OPEN)
            ws.send(data)
        else
            console.warn('! websocket not open for sending')
    }

    sendWS('\r\nATD ' + host + '\r\n')

    const client = net.createConnection({
        port: port,
        host: host
    }, () => {
        console.log('o connected to BBS')
        sendWS('+++CONNECT')

        client.on('data', (data) => {
            sendWS(data)
        })

        client.on('end', () => {
            console.log('o BBS is closing connection')
        })

        client.on('error', (error) => {
            console.log('? net.Socket error:', error)
            // sendWS(`\r\n<${host} ${error.code}>\r\n`)   disabled due to insufficient client connectivity check (closed socket)
        })

        client.on('timeout', () => {})

        client.on('close', (isError) => {
            console.log(isError ? '? BBS connection closed with errors' : 'o BBS connection successfully closed')
            if (ws.readyState === ws.OPEN)
                ws.send('+++DISCONNECT')
            callback('close')
        })
    })

    return client
}

function logStats() {
    console.log('Active connections: %s  Total connections: %s', activeConnections, totalConnections)
}

console.log(`\r\nServing app from "${staticPath}".\r\nWaiting for connection on port HTTP:${HTTPport} and WS:${WSport}`)