BUILD_DIR=__build__
DIST_DIR=${DIST:-$BUILD_DIR/dist}  # set DIST_DIR to $DIST or default
BROWSER_DIR=$DIST_DIR/browser
SOURCE=src
RESOURCES=resources

# Clean
echo Cleaning up...
if [ -d $BUILD_DIR ]; then rm -rf $BUILD_DIR; fi
mkdir -p $BROWSER_DIR

# Build
echo Building...
babel $SOURCE -d $BUILD_DIR
browserify $BUILD_DIR/main.js -o $BUILD_DIR/webterm.js

# Make dist
uglifyjs $BUILD_DIR/webterm.js --compress -o $BROWSER_DIR/webterm.js
cp $RESOURCES/* $BROWSER_DIR
cp server.js $DIST_DIR

echo
echo Distro compiled to \"$DIST_DIR\"
echo