var c64 = require('./c64');

var streamBuffer = [];
var streamerActive = false;
var baudRate = 2400;
var lastTime = undefined;

function streamOut(currentTime) {
  if (lastTime == undefined)
    lastTime = currentTime;

  var deltaS = (currentTime - lastTime) / 1000.0;
  lastTime = currentTime;
  var charsPerSecond = baudRate/9;
  var charsToEmit = Math.round(deltaS * charsPerSecond);

  if (charsToEmit==0)
    return;

  while ((charsToEmit>0) && (streamBuffer.length>0)) {
    c64.charOut(streamBuffer[0]);
    streamBuffer = streamBuffer.slice(1);
    charsToEmit--;
  }

  if (streamBuffer.length  == 0)
    streamerActive = false;
  else
    c64.cpu.irq();
}

function asciiToPetscii(code) {
  if (97 <= code && code <= 122) {
    code-= 32;
  }
  else if (65 <= code && code <= 90) {
    code+= 32;
  }
  switch (code) {
    case 0x08:
      code = 0x14; // Backspace -> DEL
      break;
  }
  return code;
}

// Output ASCII to C64 screen
exports.printAscii = function(str, shifted) {
  if (shifted)
    c64.cpu.jsr(0xffd2, {a:0x09})
  else {
    c64.cpu.jsr(0xffd2, {a:0x08})
  }
  for (let i = 0; i < str.length; i++) {
    var code = asciiToPetscii(str.charCodeAt(i) & 0xff);
    if (streamBuffer.length == 0)
      c64.charOut(code);
    else
      streamBuffer.push(code);
  }
};

exports.printPet = function(str, shifted) {
  if (shifted)
    c64.cpu.jsr(0xffd2, {a:0x09})
  else {
    c64.cpu.jsr(0xffd2, {a:0x08})
  }
  for (let i = 0; i < str.length; i++) {
    if (streamBuffer.length == 0)
      c64.charOut(str[i]);
    else
      streamBuffer.push(code);
  }
};

var streamPut = function(byte) {
  var shouldStartStreamer = streamBuffer.length == 0;
  streamBuffer.push(byte);

  if ((shouldStartStreamer) && (!streamerActive)) {
    streamerActive = true;
    lastTime = undefined;
  }
};

var streamOutput = function(buf) {
  var tmp = new DataView(buf);
  for (i=0; i<tmp.byteLength; i++)
    streamPut(tmp.getUint8(i));
};

exports.streamOutput = streamOutput;

exports.streamPut = streamPut;

exports.asciiToPetscii = asciiToPetscii;

exports.getJson = function(url) {
  var resp = '', xmlHttp;
  xmlHttp = new XMLHttpRequest();
  if(xmlHttp != null)
  {
    xmlHttp.open( "GET", url, false );
    xmlHttp.send( null );
    resp = xmlHttp.responseText;
  }
  return resp ;
}

exports.getStreamerActive = () => streamerActive;
exports.streamOut = streamOut;
