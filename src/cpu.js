var kernel = require('./kernel')
var basic = require('./basic')
require('./sprintf')

module.exports = (function() {
  var AddressMode = (function() {
    var IMP=0;
    var IMM=1;
    var ZP =2;
    var ZPX=3;
    var ZPY=4;
    var IZX=5;
    var IZY=6;
    var ABS=7;
    var ABX=8;
    var ABY=9;
    var IND=10;
    var REL=11;

    var op = [
      // x0  x1  x2  x3  x4  x5  x6  x7  x8  x9  xA  xB  xC  xD  xE  xF
        IMP,IZX,IMP,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //0x
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX, //1x
        ABS,IZX,IMP,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //2x
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX, //3x
        IMP,IZX,IMP,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //4x
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX, //5x
        IMP,IZX,IMP,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,IND,ABS,ABS,ABS, //6x
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX, //7x
        IMM,IZX,IMM,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //8x
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABY,ABY, //9x
        IMM,IZX,IMM,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //Ax
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPY,ZPY,IMP,ABY,IMP,ABY,ABX,ABX,ABY,ABY, //Bx
        IMM,IZX,IMM,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //Cx
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX, //Dx
        IMM,IZX,IMM,IZX, ZP, ZP, ZP, ZP,IMP,IMM,IMP,IMM,ABS,ABS,ABS,ABS, //Ex
        REL,IZY,IMP,IZY,ZPX,ZPX,ZPX,ZPX,IMP,ABY,IMP,ABY,ABX,ABX,ABX,ABX  //Fx
    ];

    return {
      toString:function(mode) {
        switch(mode) {
          case IMP: return "";
          case IMM: return "#$%02x";
          case  ZP: return "$%02x";
          case ZPX: return "$%02x,x";
          case ZPY: return "$%02x,y";
          case IZX: return "($%02x,x)";
          case IZY: return "($%02x),y";
          case ABS: return "$%04x";
          case ABX: return "$%04x,x";
          case ABY: return "$%04x,y";
          case IND: return "($%04x)";
          case REL: return "$%04x";
        }
        return "???";
      },

      isImmediate:function(mode) {
        return mode == IMM;
      },

      isImplied:function(mode) {
         return mode == IMP;
      },

      op:op,

      IMP:IMP,
      IMM:IMM,
      ZP :ZP,
      ZPX:ZPX,
      ZPY:ZPY,
      IZX:IZX,
      IZY:IZY,
      ABS:ABS,
      ABX:ABX,
      ABY:ABY,
      IND:IND,
      REL:REL
    };
  })();

  var Instruction = (function() {
    var m = {
      BRK:"brk",ORA:"ora",KIL:"kil",SLO:"slo",NOP:"nop",ASL:"asl",ROL:"rol",PHP:"php",
      BMI:"bmi",ANC:"anc",BPL:"bpl",CLC:"clc",JSR:"jsr",AND:"and",RLA:"rla",BIT:"bit",
      PLP:"plp",SEC:"sec",RTI:"rti",EOR:"eor",SRE:"sre",LSR:"lsr",PHA:"pha",ALR:"alr",
      JMP:"jmp",BVC:"bvc",STA:"sta",STX:"stx",STY:"sty",SAX:"sax",CPX:"cpx",DEC:"dec",
      DCP:"dcp",ISC:"isc",SBC:"sbc",CMP:"cmp",INC:"inc",SED:"sed",CLD:"cld",CPY:"cpy",
      DEX:"dex",LAX:"lax",BEQ:"beq",INX:"inx",AXS:"axs",LDX:"ldx",LDY:"ldy",TXA:"txa",
      INY:"iny",LDA:"lda",TSX:"tsx",LAS:"las",TAX:"tax",AHX:"ahx",SHX:"shx",CLV:"clv",
      BNE:"bne",BCS:"bcs",TAY:"tay",TAS:"tas",TXS:"txs",TYA:"tya",BCC:"bcc",DEY:"dey",
      RRA:"rra",ROR:"ror",SHY:"shy",XAA:"xaa",ADC:"adc",SEI:"sei",BVS:"bvs",RTS:"rts",
      CLI:"cli",PLA:"pla",ARR:"arr"
    };

    var op = [
      // x0  x1  x2  x3  x4  x5  x6  x7  x8  x9  xA  xB  xC  xD  xE  xF
      m.BRK,m.ORA,m.KIL,m.SLO,m.NOP,m.ORA,m.ASL,m.SLO,m.PHP,m.ORA,m.ASL,m.ANC,m.NOP,m.ORA,m.ASL,m.SLO, //0x
      m.BPL,m.ORA,m.KIL,m.SLO,m.NOP,m.ORA,m.ASL,m.SLO,m.CLC,m.ORA,m.NOP,m.SLO,m.NOP,m.ORA,m.ASL,m.SLO, //1x
      m.JSR,m.AND,m.KIL,m.RLA,m.BIT,m.AND,m.ROL,m.RLA,m.PLP,m.AND,m.ROL,m.ANC,m.BIT,m.AND,m.ROL,m.RLA, //2x
      m.BMI,m.AND,m.KIL,m.RLA,m.NOP,m.AND,m.ROL,m.RLA,m.SEC,m.AND,m.NOP,m.RLA,m.NOP,m.AND,m.ROL,m.RLA, //3x
      m.RTI,m.EOR,m.KIL,m.SRE,m.NOP,m.EOR,m.LSR,m.SRE,m.PHA,m.EOR,m.LSR,m.ALR,m.JMP,m.EOR,m.LSR,m.SRE, //4x
      m.BVC,m.EOR,m.KIL,m.SRE,m.NOP,m.EOR,m.LSR,m.SRE,m.CLI,m.EOR,m.NOP,m.SRE,m.NOP,m.EOR,m.LSR,m.SRE, //5x
      m.RTS,m.ADC,m.KIL,m.RRA,m.NOP,m.ADC,m.ROR,m.RRA,m.PLA,m.ADC,m.ROR,m.ARR,m.JMP,m.ADC,m.ROR,m.RRA, //6x
      m.BVS,m.ADC,m.KIL,m.RRA,m.NOP,m.ADC,m.ROR,m.RRA,m.SEI,m.ADC,m.NOP,m.RRA,m.NOP,m.ADC,m.ROR,m.RRA, //7x
      m.NOP,m.STA,m.NOP,m.SAX,m.STY,m.STA,m.STX,m.SAX,m.DEY,m.NOP,m.TXA,m.XAA,m.STY,m.STA,m.STX,m.SAX, //8x
      m.BCC,m.STA,m.KIL,m.AHX,m.STY,m.STA,m.STX,m.SAX,m.TYA,m.STA,m.TXS,m.TAS,m.SHY,m.STA,m.SHX,m.AHX, //9x
      m.LDY,m.LDA,m.LDX,m.LAX,m.LDY,m.LDA,m.LDX,m.LAX,m.TAY,m.LDA,m.TAX,m.LAX,m.LDY,m.LDA,m.LDX,m.LAX, //Ax
      m.BCS,m.LDA,m.KIL,m.LAX,m.LDY,m.LDA,m.LDX,m.LAX,m.CLV,m.LDA,m.TSX,m.LAS,m.LDY,m.LDA,m.LDX,m.LAX, //Bx
      m.CPY,m.CMP,m.NOP,m.DCP,m.CPY,m.CMP,m.DEC,m.DCP,m.INY,m.CMP,m.DEX,m.AXS,m.CPY,m.CMP,m.DEC,m.DCP, //Cx
      m.BNE,m.CMP,m.KIL,m.DCP,m.NOP,m.CMP,m.DEC,m.DCP,m.CLD,m.CMP,m.NOP,m.DCP,m.NOP,m.CMP,m.DEC,m.DCP, //Dx
      m.CPX,m.SBC,m.NOP,m.ISC,m.CPX,m.SBC,m.INC,m.ISC,m.INX,m.SBC,m.NOP,m.SBC,m.CPX,m.SBC,m.INC,m.ISC, //Ex
      m.BEQ,m.SBC,m.KIL,m.ISC,m.NOP,m.SBC,m.INC,m.ISC,m.SED,m.SBC,m.NOP,m.ISC,m.NOP,m.SBC,m.INC,m.ISC  //Fx
    ];

    var retval = {
      op:op
    };
    for (var i in m)
      retval[i] = m[i];
    return retval;
  })();

  var pc = 0;
  var data,ir,adl,adh,pcl,pch;
  var status=0x24;
  var sp=0;
  var a=0;
  var x=0;
  var y=0;
  var dataBus=0;
  var memory = [];
  var traps = [];
  var trapWrites = [];

  for (var i=0; i<65536; i++)
    memory[i]=0xff;

  function r(address) {
    if ((address>=0xa000) && (address<=0xbfff))
      dataBus = basic[address-0xa000];
    else if ((address>=0xe000) && (address<=0xffff))
      dataBus = kernel[address-0xe000];
    else if (address == 0xd012)
      dataBus=0;
    else if (address == 0xd019)
      dataBus=0;
    else if (address == 0xdd00)
      dataBus = (memory[address&0xffff]|0x40) & 0xff;
    else if (address == 0xdc01)
      dataBus=0xff;
    else
      dataBus = memory[address&0xffff] & 0xff;

    return dataBus;
  }

  function r16(address) {
    var retv=0;
    retv|=r(address+0);
    retv|=r(address+1)<<8;
    return retv;
  }

  function r16p(address) {
    var retv=0;

    var lo = address;
    var hi = (lo&0xff00) | ((lo+1)&0xff);
    retv |= r(lo);
    retv |= r(hi)<<8;

    return retv;
  }

  function w(address, value) {
    value&=0xff;
    dataBus = value;

    memory[address&0xffff] = dataBus;

    for (var i=0; i<trapWrites.length; i++) {
      var range = trapWrites[i];
      if ((address>=range[0]) && (address<=range[1]))
        range[2](address, value);
    }
  }

  function reset() {
    var resetVector = r16(0xfffc);
    sp -= 3;
    sp &= 0xff;
    status |= 4;
    pc = resetVector;
  }

  function updateV(value) {
    status &= ~0x40;
    if ((value<-128) || (value>127))
      status |= 0x40;
  }

  function updateNZ(value) {
    status &= ~0x82;
    if (value==0)
      status |= 2;
    if (value>=0x80)
      status |= 0x80;
  }

  function branch(takeBranch) {
      if (!takeBranch)
        return;

      pc += rel;
  }

    // OPERATIONS --------------------------------------------------------------------------
  function lda(value) {
      a = value&0xff;
      updateNZ(a);
  }

  function ldx(value) {
    x = value&0xff;
    updateNZ(x);
  }

  function ldy(value) {
    y = value&0xff;
    updateNZ(y);
  }

  function lax(value) {
    a = x = value&0xff;
    updateNZ(a);
  }

  function cpx(value) {
    updateNZ((x-value)&0xff);
    status&=~1;
    if (x>=value)
      status|=1;
  }

  function cpy(value) {
    updateNZ((y-value)&0xff);
    status&=~1;
    if (y>=value)
      status|=1;
  }

  function cmp(value) {
    updateNZ((a-value)&0xff);
    status&=~1;
    if (a>=value)
      status|=1;
  }

  function ora(value) {
    a |= value;
    updateNZ(a);
  }

  function eor(value) {
    a ^= value;
    updateNZ(a);
  }

  function and(value) {
    a &= value;
    updateNZ(a);
  }

  function bit(value) {
    updateNZ(a&value);
    status &= ~0xc0;
    status |= value&0xc0;
  }

  function asl(value) {
    status &= ~1;
    status |= (value&0x80)>>7;
    value <<= 1;
    value &= 0xff;
    updateNZ(value);
    return value;
  }

  function rol(value) {
    var c = status&1;
    value = asl(value);
    value |= c;
    updateNZ(value);
    return value;
  }

  function lsr(value) {
    status &= ~1;
    status |= value&1;
    value >>= 1;
    value &= 0xff;
    updateNZ(value);
    return value;
  }

  function ror(value) {
    var c = status&1;
    value = lsr(value);
    value |= c<<7;
    updateNZ(value);
    return value;
  }

  function dec(value) {
    value--;
    value&=0xff;
    updateNZ(value);
    return value;
  }

  function inc(value) {
    value++;
    value&=0xff;
    updateNZ(value);
    return value;
  }

  function asl_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = asl(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function rol_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = rol(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function lsr_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = lsr(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function ror_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = ror(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function dec_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = dec(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function inc_rmw(addr) {
    var v,ov;
    v = ov = r(addr);
    v = inc(v);
    w(addr, ov);
    w(addr, v);
    return v;
  }

  function signExtend(v) {
    // 0..127 => 0..127
    // 128..255 => -128..-1

    v &= 0xff;
    if (v<=127)
      return v;
    return -(256-v);
  }

  function adc(v) {
    updateV(signExtend(a) + signExtend(v) + (status&1));
    a += v+(status&1);
    status&=~1;
    if (a>255)
      status|=1;
    a &= 0xff;
    updateNZ(a);
  }

  function sbc(v) {
    updateV(signExtend(a) - signExtend(v) - ((status&1)^1));
    a -= v+((status&1)^1);
    status&=~1;
    if (a>=0)
      status|=1;
    a &= 0xff;
    updateNZ(a);
  }

  function brk() {
  }

  function php() {
    push(status|0x10);
  }

  function jsr() {
    push((pc-1)>>8);
    push((pc-1)&0xff);
    pc=addr;
  }
  var realjsr=jsr;

  function plp() {
    status = pop()&(~0x10);
  }

  function rti() {
    var address = 0;
    status = pop();
    address |= pop();
    address |= pop()<<8;

    pc=address;
  }

  function push(value) {
    w(0x100+sp, value&0xff);
    sp--;
    sp&=0xff;
  }

  function pop() {
    sp++;
    sp&=0xff;
    return r(0x100+sp);
  }

  function rts() {
    var address = 0;
    address |= pop();
    address |= pop()<<8;
    pc=address+1;
  }

  function pla() {
    a = pop();
    updateNZ(a);
  }

  // ADDRESS MODES ---------------------------------------------------------------
  var addr; // Internal ADL,ADH register
  var opAddr; // The actual adress used in the op-code
  var opValue; // The actual immediate used in the op-code
  var rel;

  function abs() {
    var lo = dataBus;
    var hi = r(pc+1);
    pc+=2;
    addr=opAddr=lo|(hi<<8);
    return opAddr;
  }

  function imm() {
    opValue = dataBus;
    pc++;
    return dataBus;
  }

  function zp() {
    addr=opAddr=dataBus;
    pc++;
    return opAddr;
  }

  function zpx() {
    zp();
    addr+=x;
    return addr;
  }

  function zpy() {
    zp();
    addr+=y;
    return addr;
  }

  function abx() {
    addr = abs();
    addr += x;
    return addr;
  }

  function aby() {
    addr = abs();
    addr += y;
    return addr;
  }

  function izx() {
    zp();
    var offset = (addr+x)&0xff;
    addr = r16p(offset);
    return addr;
  }

  function izy() {
    zp();
    addr = r16p(addr);
    addr += y;
    return addr;
  }

  function _rel() {
    pc++;
    rel = signExtend(dataBus);
    return rel;
  }

  function ind() {
    abs();
    addr = r16p(addr);
    return addr;
  }

  // Instruction decoder
  var lastPC=-1;
  var idle=false;
  var debug=false;
  var debugOut="";

  function execute() {
    status |= 0x20;

    if ((a>=0x100) || (a<0)) {
      console.log("Internal CPU-error. a="+a);
      return false;
    }
    if ((x>=0x100) || (x<0)) {
      console.log("Internal CPU-error. x="+x);
      return false;
    }
    if ((y>=0x100) || (y<0)) {
      console.log("Internal CPU-error. y="+y);
      return false;
    }

    if ((y>=0x100) || (y<0)) {
      console.log("Internal CPU-error. PC wrap around");
      return false;
    }

    if (pc==lastPC) {
      if (debug && !idle)
        console.log("IDLE");
      idle=true;
    }
    else
      idle=false;
    lastPC=pc;

    if (traps[pc] !== undefined)
      traps[pc]({a:a, x:x, y:y});

    var opcode = r(pc);
    var instruction = Instruction.op[opcode];
    var addressMode = AddressMode.op[opcode];

    var cpuState="";
    var debugLine="";
    if (debug && !idle) {
      cpuState = sprintf("A:%02X X:%02X Y:%02X P:%02X SP:%02X", a,x,y,status,sp);
      debugLine = sprintf("%04X  %02X ", pc, opcode);
      while(debugLine.length<16)
        debugLine+=" ";
    }
    pc++;

    r(pc);

    if (debug && !idle)
      debugLine += instruction + " ";

    switch(addressMode) {
      case AddressMode.IMP: break;
      case AddressMode.IMM: imm(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), dataBus); break;
      case AddressMode.ZP:  zp();  if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.ZPX: zpx(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.ZPY: zpy(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.IZX: izx(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.IZY: izy(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.ABS: abs(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.ABX: abx(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.ABY: aby(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.IND: ind(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), opAddr); break;
      case AddressMode.REL: _rel(); if (debug && !idle) debugLine += sprintf(AddressMode.toString(addressMode), (pc+rel)&0xffff); break;
    }

    switch(instruction) {
      case Instruction.BRK: brk();
        return false;
        break;
      case Instruction.ORA: if (!AddressMode.isImmediate(addressMode)) r(addr);
        ora(dataBus);
        break;
      case Instruction.KIL:
        debugLine += (pc-1).toString(16)+"   CPU Jam";
        console.log(debugLine);
        return false;
      case Instruction.SLO: ora(asl_rmw(addr)); break;
      case Instruction.NOP:
        if (AddressMode.isImplied(addressMode))
          break;
        if (!AddressMode.isImmediate(addressMode))
          r(addr);
        break;
      case Instruction.ASL:
        if (AddressMode.isImplied(addressMode))
          a=asl(a);
        else
          asl_rmw(addr);
        break;
      case Instruction.ROL:
        if (AddressMode.isImplied(addressMode))
          a=rol(a);
        else
          rol_rmw(addr);
        break;
      case Instruction.PHP: php(); break;
      case Instruction.BMI: branch((status & 0x80) != 0); break;
      case Instruction.BPL: branch((status & 0x80) == 0); break;
      case Instruction.CLC: status &= ~1; break;
      case Instruction.JSR: jsr(); break;
      case Instruction.AND: if (!AddressMode.isImmediate(addressMode)) r(addr);
        and(dataBus);
        break;
      case Instruction.RLA: and(rol_rmw(addr)); break;
      case Instruction.BIT: bit(r(addr)); break;
      case Instruction.PLP: plp(); break;
      case Instruction.SEC: status|=1; break;
      case Instruction.RTI: rti(); break;
      case Instruction.EOR: if (!AddressMode.isImmediate(addressMode)) r(addr);
        eor(dataBus);
        break;
      case Instruction.SRE: eor(lsr_rmw(addr)); break;
      case Instruction.LSR:
        if (AddressMode.isImplied(addressMode))
          a=lsr(a);
        else
          lsr_rmw(addr);
        break;
      case Instruction.PHA: push(a); break;
      case Instruction.ALR: and(dataBus); lsr(a); break;
      case Instruction.JMP: pc=addr; break;
      case Instruction.BVC: branch((status & 0x40) == 0); break;
      case Instruction.STA: w(addr, a); break;
      case Instruction.STX: w(addr, x); break;
      case Instruction.STY: w(addr, y); break;
      case Instruction.SAX: w(addr, a&x); break;
      case Instruction.CPX: if (!AddressMode.isImmediate(addressMode)) r(addr);
        cpx(dataBus);
        break;
      case Instruction.DEC: dec_rmw(addr); break;
      case Instruction.DCP: cmp(dec_rmw(addr)); break;
      case Instruction.ISC: sbc(inc_rmw(addr)); break;
      case Instruction.SBC: if (!AddressMode.isImmediate(addressMode)) r(addr);
        sbc(dataBus);
        break;
      case Instruction.CMP: if (!AddressMode.isImmediate(addressMode)) r(addr);
        cmp(dataBus);
        break;
      case Instruction.INC: inc_rmw(addr); break;
      case Instruction.SED: status |= 8; break;
      case Instruction.CLD: status &= ~8; break;
      case Instruction.CPY: if (!AddressMode.isImmediate(addressMode)) r(addr);
        cpy(dataBus);
        break;
      case Instruction.DEX: x = dec(x); break;
      case Instruction.LAX: if (!AddressMode.isImmediate(addressMode)) r(addr);
        lax(dataBus);
        break;
      case Instruction.BEQ: branch((status & 2) != 0); break;
      case Instruction.INX: x = inc(x); break;
      case Instruction.AXS: x = (a&x)-dataBus; x&=0xff; updateNZ(x); break; // TODO this is wrong
      case Instruction.ANC: and(dataBus); status &= ~1; status|=(a>>7)&1; break;
      case Instruction.LDX: if (!AddressMode.isImmediate(addressMode)) r(addr);
        ldx(dataBus);
        break;
      case Instruction.LDY: if (!AddressMode.isImmediate(addressMode)) r(addr);
        ldy(dataBus);
        break;
      case Instruction.TXA: a=x; updateNZ(a); break;
      case Instruction.INY: y = inc(y); break;
      case Instruction.LDA: if (!AddressMode.isImmediate(addressMode)) r(addr);
        lda(dataBus);
        break;
      case Instruction.TSX: x=sp; updateNZ(x); break;
      case Instruction.LAS: a=x=sp=r(addr)&sp; updateNZ(a); break;
      case Instruction.TAX: x=a; updateNZ(x); break;
      case Instruction.AHX: w(addr, a&x&(addr>>8)); break;
      case Instruction.SHX: w(addr, y&(addr>>8)); break;
      case Instruction.CLV: status &= ~0x40; break;
      case Instruction.BNE: branch((status & 2) == 0); break;
      case Instruction.BCS: branch((status & 1) != 0); break;
      case Instruction.TAY: y=a; updateNZ(y); break;
      case Instruction.TAS: sp = a&x; w(addr, sp&(addr>>8)); break;
      case Instruction.TXS: sp=x; updateNZ(sp); break;
      case Instruction.TYA: a=y; updateNZ(y); break;
      case Instruction.BCC: branch((status & 1) == 0); break;
      case Instruction.DEY: y = dec(y); break;
      case Instruction.RRA: adc(ror_rmw(addr)); break;
      case Instruction.ROR:
        if (AddressMode.isImplied(addressMode))
          a=ror(a);
        else
          ror_rmw(addr);
        break;
      case Instruction.SHY: w(addr, y&(addr>>8));break;
      case Instruction.XAA: a=x&dataBus; updateNZ(a); break;
      case Instruction.ADC: if (!AddressMode.isImmediate(addressMode)) r(addr);
        adc(dataBus);
        break;
      case Instruction.SEI: status|=4; break;
      case Instruction.BVS: branch((status & 0x40) != 0); break;
      case Instruction.RTS: rts(); break;
      case Instruction.CLI: status&=~4; break;
      case Instruction.PLA: pla(); break;
      case Instruction.ARR: and(dataBus); ror(a); break;
      default:
        debugLine += (pc-1).toString(16)+"   Unimplemented opcode 0x"+opCode.toString(16);
        console.log(debugLine);
        return false;
    }

    if ((debug) && (!idle)) {
     while(debugLine.length<48)
        debugLine+=" ";
      debugLine+=cpuState;
      debugLine=debugLine.toUpperCase();
      debugLine+="\n";
      debugOut+=debugLine;
    }

    return true;
  }

  return {
    memory:memory,
    reset:reset,
    execute:execute,
    debug:function(newDebug) {
      var out = debugOut;
      if (newDebug !== undefined) {
        debugOut = "";
        debug = newDebug;
      }
      return out;
    },
    pc:function(newPc) {
      pc = newPc;
    },
    trap:function(address, callback) {
      traps[address] = callback;
    },
    trapWrites:function(startAdress, stopAddress, callback) {
      trapWrites.push([startAdress, stopAddress, callback]);
    },
    poke:function(address, value) {
      w(address, value);
    },
    irq:function() {
      if ((status&4) == 4)
        return;

      var startsp = sp;

      addr = r16p(0xfffe);
      push((pc-1)>>8);
      push((pc-1)&0xff);
      pc=addr;
      push(status);
      status|=4;

      while (sp != startsp) {
        if (!execute()) {
          console.log("Problem");
          break;
        }
      }
      pc++;
    },
    runInstructions:function(nbr) {
      while (nbr>0) {
        execute();
        nbr--;
      }
    },
    jsr:function(address, regs) {
      var startsp = sp;
      addr = address;

      if (regs !== undefined) {
        if (regs.a !== undefined)
          a = regs.a;
        if (regs.x !== undefined)
          x = regs.x;
        if (regs.y !== undefined)
          y = regs.y;
      }

      realjsr();

      while (sp != startsp) {
        if (!execute()) {
          console.log("Problem");
          return false;
        }
      }
      return {a:a, x:x, y:y};
    }
  }
})();
