var ui = require('./ui');
var utils = require('./utils');
var Colodore = require('./colodore-gl');
var c64 = require('./c64');

var WSport = 27561

module.exports = function() {
  const ctrlKeymap = [0x92, 0x90, 0x05, 0x1c, 0x9f, 0x9c, 0x1e, 0x1f, 0x9e, 0x12];  //rvs off, blk, wht, red, cyn, pur, grn, blu, yel, rvs on
  const cmdKeymap = [0x81, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b];  //orng, brn, lred, dgry, mgry, lgrn, lblu, lgry
  // const col = ["\x90","\x05","\x1c","\x9f","\x9c","\x1e","\x1f","\x9e","\x81","\x95","\x96","\x97","\x98","\x99","\x9a","\x9b"];
  var currentBBS;

  // Reset kernel and screen editor
  c64.cpu.jsr(0xff87);
  c64.cpu.jsr(0xff84);
  c64.cpu.jsr(0xff8a);
  c64.cpu.jsr(0xffe7);
  c64.cpu.jsr(0xff5b);
  c64.cpu.poke(0xd020, 11);
  c64.cpu.poke(0xd021, 0);

  utils.printAscii( "\r\n   \x99*\x9f*\x9a*\x9c* \x9fWelcome to \x9eW\x99e\x9bb\x9fT\x9be\x99r\x9em \x9f0.40\x99 \x9c*\x9a*\x9f*\x99*"+
                    "\r\n   \x99C\x9fo\x9ade by\x12\x1c\xa1\x81R\x96a\x9ev\x05eG\x9eu\x96r\x81u\x92\x1c\xa1\x9e&\x12\x1f\xa1\x9cJ\x9aa\x9fc\x9ek\x05A\x9es\x9fs\x9ae\x9cr\x92\x1f\xa1\x9a20\x9f1\x997"+
                    "\r\n           \x9eC\x99o\x9fl\x1eo\x98dore by \x98P\x1ee\x9fp\x99t\x9eo\r\n\r\n");

  // Enable cursor and fake CIA-timer. :)
  c64.cpu.memory[0xcc]=0;
  var lastIRQtime = undefined;
  var currentTime;
  var idleSince = undefined;
  var screenSaverPictureSince = undefined;
  var go64mode = false;

  function r(time) {
    var timerValue = c64.cpu.memory[0xdc04] | c64.cpu.memory[0xdc05]*256;
    var cpuFrequency = 985248;
    var interval = Math.floor((timerValue*1000) / cpuFrequency);

    currentTime = time;
    if (idleSince === undefined)
      idleSince = currentTime;

    // Screen saver after 60s, then new picture every 10s
    if (!go64mode) {
      if ((currentTime-idleSince > 60000) && !c64.screenSaver()) {
        screenSaverPictureSince = currentTime;
        c64.screenSaver(true);
      }
      if ((currentTime-screenSaverPictureSince > 10000) && c64.screenSaver()) {
        screenSaverPictureSince = currentTime;
        c64.screenSaver(true);
      }
    }

    if (lastIRQtime === undefined)
      lastIRQtime = currentTime;

    var deltaS = (currentTime - lastIRQtime);
    if (deltaS>=interval) {
      lastIRQtime = currentTime;
      c64.cpu.irq();
    }
    if (go64mode)
      c64.cpu.runInstructions(5000);

    if (utils.getStreamerActive())
      utils.streamOut(currentTime);

    if (c64.charsUpdated())
      Colodore.repaint();

    window.requestAnimationFrame(r);
  };
  window.requestAnimationFrame(r);

  // Trap scroll
  c64.cpu.trap(0xe8f6, function() {
    c64.snapshotFirstRow();
    var scroller = document.getElementById('scroller');
    var height = parseInt(scroller.style.height);
    height += 3*8*1;
    scroller.style.height = height+"px";
    scroller.scrollIntoView(false);
  });

  // Trap clear screen
  c64.cpu.trap(0xe544, function() {
    c64.snapshotScreen();
    var scroller = document.getElementById('scroller');
    var height = parseInt(scroller.style.height);
    height += 3*8*25;
    scroller.style.height = height+"px";
    scroller.scrollIntoView(false);
  });

  // setup UI stuff
  const options = document.querySelector('#hostlist')
  ui.bbsList(options, dialHandler(options));
  ui.dialButton(dialHandler(options));
  ui.hangupButton((e) => ws.send('+++HANGUP'))

  ui.downloadButton((e) => {
    currentStateMachine = require('./punterstatemachine')(keySend, function() {
      console.log("Error! Switching back to normal");
      currentStateMachine = require('./normalstatemachine')(keySend);
      currentIterator = currentStateMachine.begin();
    });
    currentIterator = currentStateMachine.begin();
  });

  var sliders = document.getElementsByClassName("slider");
  for (var i=0; i<sliders.length; i++) {
    var slider = sliders[i];
    slider.addEventListener('mousemove', handleSlider)
    slider.addEventListener('change', handleSlider)
  }

  const title = document.querySelector('title');

  document.addEventListener('keypress', keypressHandler);
  document.addEventListener('keydown', keydownHandler);

  // Websocket stuff
  // const ws = new WebSocket('wss://webterm.retrohackers.com/ws')
  const ws = go64mode ? {} : new WebSocket('ws://'+window.location.hostname+':'+WSport);
  ws.binaryType = "arraybuffer";

  ws.onopen = (e) => {
    console.log('Modem connected');
    ws.send('+++ATZ');
  }

  var currentStateMachine = require('./normalstatemachine')(keySend);
  var currentIterator = currentStateMachine.begin();
  ws.onmessage = (e) => {
    if (!e.data)
      return;
    var data = e.data;
    if (typeof data === 'string') {
      if (data.indexOf('+++') == 0) {
        // Control message
        data = e.data.slice(3)
        switch (data) {
          case 'CONNECT':
            ui.muteButton('a#dial');
            ui.unmuteButton('a#hangup');
            title.innerHTML = currentBBS;
            c64.screenSaver(false);
            break;
          case 'DISCONNECT':
            ui.unmuteButton('a#dial');
            ui.muteButton('a#hangup');
            currentBBS = null;
            title.innerHTML = 'WebTerm';
            data = 'NO CARRIER'
            break;
          default:
        }
        utils.printAscii('\r\n\r\n\x98\x92'+data+'\r\n')
      }
      else {
        // message from server
        utils.printAscii('\x92\x98'+data);
      }
      return;
    }
    // utils.printPet(new Uint8Array(data));

    // Generate data as byte stream
    var tmp = new DataView(data);
    for (i=0; i<tmp.byteLength; i++) {
      var retval = currentIterator.next(tmp.getUint8(i));
      if (retval.done) {
        console.log("Current statemachine done, switching to normal");
        currentStateMachine = require('./normalstatemachine')(keySend);
        currentIterator = currentStateMachine.begin();
        i--;
        continue;
      }
    }
  }

  ws.onerror = (e) => {
    console.warn('?WS ERROR ', e);
    utils.printAscii('\r\n\x98\x92?NETWORK ERROR\r\nREADY.\r\n');
  }

  ws.onclose = (e) => {
    console.warn('?WS CLOSED ', e);
    utils.printAscii('\r\n\x98\x92?MODEM CONNECTION DROPPED\r\nPLEASE RELOAD!\r\nREADY.\r\n')
  }

  function dialHandler(list) {
    return function (e) {
      if (ws.readyState !== ws.OPEN)
        return;
      ws.send('+++DIAL '+list.options[list.selectedIndex].value);
      currentBBS = list.options[list.selectedIndex].text;
      e.target.blur();
    }
  }

  function getKeyCode(e) {
    if (e.which == null) {
      return e.keyCode // IE
    }
    else if (e.which!=0 && e.charCode!=0) {
      return e.which   // the rest
    }
    else {
      return null // special key
    }
  }

  function go64() {
    go64mode = true;
    c64.screenSaver(false);
    c64.cpu.reset();
    c64.cpu.runInstructions(600000);

    var chaosprg = require('./chaosprg');
    for (var i=2; i<chaosprg.length; i++)
      c64.memory[0x0801+i-2] = chaosprg[i];

    var end = 0x0801+chaosprg.length-2;
    c64.memory[0x2d] = end&0xff;
    c64.memory[0x2e] = (end>>8)&0xff;
    c64.cpu.jsr(0xa52a);
  }
  window.go64 = go64;

  function keySend(v) {
    if ((ws.readyState == ws.OPEN) && (!go64mode))
      ws.send(v);
    else if (go64mode) {
      var size = c64.memory[0xc6];
      if (size<c64.memory[0x289]) {
        c64.memory[0x277+size] = v;
        c64.memory[0xc6] = size+1;
      }
    }
  }

  function keypressHandler(e) {
    idleSince = currentTime;

    if (c64.screenSaver())
      return;

    // only 'keypress' event has normalized keycodes
    var code = getKeyCode(e);

    currentStateMachine.keyPressed(code);
  };

  function keydownHandler(e) {
    idleSince = currentTime;

    if ((e.keyCode==13) && (e.altKey)) {
      var elem = document.getElementById("framebuffer");
      if (elem.requestFullScreen)
        elem.requestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      else if (elem.mozRequestFullScreen)
        elem.mozRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      else if (elem.webkitRequestFullScreen)
        elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      e.preventDefault();
      return;
    }

    if (c64.screenSaver()) {
      if (e.keyCode == 32) {
        c64.screenSaver(false);
        e.preventDefault();
      } else if (e.keyCode == 27) {
        screenSaverPictureSince = currentTime;
        c64.screenSaver(true);
        e.preventDefault();
      }
      return;
    }

    var code;
    c64.memory[0x91] = 0;
    switch (e.keyCode) {
      case 27: // esc
        if (go64mode) {
          code = 0x03; // run-stop (TODO: not working though, probably need some proper CIA handling since it's closly related to restore)
          c64.memory[0x91] = 0x7f;
          break;
        }
        else {
          screenSaverPictureSince = currentTime;
          c64.screenSaver(true);
          e.preventDefault();
          return;
        }
      case 46: // delete => insert
        code = 0x94;
        break;
      case 8: // backspace (del)
        code = 0x14;
        break;
      case 9: // tab (control)
        e.preventDefault();
        break;
      case 13: // return
        code = 0x0d;
      case 16: // shifted
        break;
      case 17: // ctrl (C=)
        break;
      case 40: // crsr down
        code = 0x11;
        break;
      case 38: // crsr up
        code = 0x91;
        break;
      case 37: // crsr left
        code = 0x9d;
        break;
      case 39: // crsr right
        code = 0x1d;
        break;
      case 52: // Convert € => $
        if (e.shiftKey)
          code = 0x24;
        break;
    }
    // handle color and rvs on/off key combos
    if (e.ctrlKey && (48 <= e.keyCode) && (e.keyCode <= 57)) {
      //console.log('ctrl +', e.keyCode-48);
      code = ctrlKeymap[e.keyCode-48];
    }
    if (e.altKey && (49 <= e.keyCode) && (e.keyCode <= 56)) {
      //console.log('cmd +', e.keyCode-48);
      code = cmdKeymap[e.keyCode-48]
    }

    // toggle shifted charset
    if (e.altKey && e.shiftKey)
      c64.cpu.poke(0x0291, c64.memory[0x0291]^0x80);

    if (code !== undefined) {
      e.preventDefault(); // suppress keypressHandler event
      keySend(code);      // send to bbs
    }
  };

  function handleSlider () {
    c64.setProperty(this.name, this.value);
  }
}
