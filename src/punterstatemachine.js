var sendByte;
var bail;

function send(str) {
    console.log("sending "+str);
    for (var i=0; i<str.length; i++)
        sendByte(str.charCodeAt(i) & 0xff);
}

function* expect(what) {
    console.log("expecting "+what);
    for (var i=0; i<what.length; i++) {
        var gotByte = yield;
        var expectByte = what.charCodeAt(i) & 0xff;
        if (gotByte != expectByte)
            throw new Error("Unexpected byte. Expected " + expectByte + ", got " + gotByte);
    }
}

function* read(length) {
    console.log("reading "+length+" bytes");
    var str="";
    for (var i=0; i<length; i++)
        str += String.fromCharCode(yield);
    return str;
}

function* expectPacket(payloadLength) {
    console.log("expect packet of length "+payloadLength);

    var packet = {
        additiveChecksum:0,
        crcChecksum:0,
        nextBlockSize:0,
        blockNumber:0,
        payload:[]
    };

    packet.additiveChecksum = yield;
    packet.additiveChecksum |= (yield) << 8;
    packet.crcChecksum = yield;
    packet.crcChecksum |= (yield) << 8;
    packet.nextBlockSize = yield;
    packet.blockNumber = yield;
    packet.blockNumber |= (yield) << 8;

    for (var i=0; i<payloadLength; i++)
        packet.payload.push(yield);

    return packet;
}

function* retry(count, func) {
    var left=count;
    while (left>0) {
        try {
            yield* func();
            break;
        }
        catch (e) {
            console.log(e);
            left--;
            continue;
        }
    }
    if (left == 0)
        throw new Error("Aborting after " + count + " failed attempts.");
}

function* timeout(ms, func) {
    var timer = setTimeout(function() {
        if (bail) bail(new Error("Timeout after " + ms + "ms."));
    }, ms);

    try {
        yield* func();
    } finally {
        clearTimeout(timer);
    }
}

function* iterator() {
    console.log("Punter - Stage 1.1");
    yield* retry(3, function*() {
        while(true) {
            send("GOO");
            var tmp = "";
            yield* timeout(2000, function*() {
                tmp = yield* read(3);
            });
            if (tmp === "ACK")
                break;
            if (tmp === "GOO")
                continue;
            throw new Error("Unexpected response "+tmp);
        }
    });

    console.log("Punter - Stage 1.2");
    send("S/B");
    var fileTypePacket = undefined;
    yield* timeout(2000, function*() {
        fileTypePacket = yield* expectPacket(1);
    });

    console.log("Punter - Stage 1.3");
    send("GOO");
    yield* expect("ACK");

    console.log("Punter - Stage 2.1");
    send("S/B");
    yield* expect("SYN");

    console.log("Punter - Stage 2.2");
    send("SYN");
    yield* expect("S/B");

    console.log("Punter - Stage 3.1");
    send("GOO");
    yield* expect("ACK");

    console.log("Punter - Stage 3.2");
    send("S/B");
    var dummyPacket = yield* expectPacket(0);

    console.log("Punter - Stage 3.3");
    send("GOO");
    yield* expect("ACK");

    var nextSize = dummyPacket.nextBlockSize;
    while(true) {
        console.log("Punter - Stage 4.1");
        send("S/B");
        var dataPacket = yield* expectPacket(nextSize);
        console.log("Got block #"+dataPacket.blockNumber);

        console.log("Punter - Stage 4.2")
        send("GOO");
        yield* expect("ACK");

        nextSize = dataPacket.nextSize;
        if ((nextSize == 0) || (dataPacket.blockNumber>=0xff00))
            break;
    }

    console.log("Punter - Stage 5.1");
    send("S/B");
    yield* expect("SYN");

    console.log("Punter - State 5.2");
    send("SYN");
    yield* expect("S/B");

    console.log("Punter completed!");
}

function keyPressed(keyCode) {
}

module.exports = function(_sendByte, _bail) {
    sendByte = _sendByte;
    bail = _bail;

    return {
        begin: function() {
            console.log("StateMachine: punterstatemachine");
            var tmp = iterator();
            tmp.next();
            return tmp;
        },
        keyPressed: keyPressed
    };
};
