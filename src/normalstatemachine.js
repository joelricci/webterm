var utils = require('./utils');
var sendByte;

function* iterator() {
    while(true) {
        var byte = yield;
        utils.streamPut(byte);
    }
}

function keyPressed(keyCode) {
    sendByte(utils.asciiToPetscii(keyCode));
}

module.exports = function(_sendByte) {
    sendByte = _sendByte;

    return {
        begin: function() {
            console.log("StateMachine: normalstatemachine");
            var tmp = iterator();
            tmp.next();
            return tmp;
        },
        keyPressed: keyPressed
    };
};
