var getJson = require('./utils').getJson;

exports.bbsList = function (list, handler) {
  console.log('Reading BBS list - hang tight...');
  const bbslist = JSON.parse(getJson('bbslist.json')).bbs_list;  //TODO: use fetch api
  list.addEventListener('change', handler)
  list.innerHTML =
    '<optgroup label="C64 Boards">'
    +bbslist
      .sort((a,b) => a.bbs_name.trim() < b.bbs_name.trim() ? -1 : 1)
      .reduce((p,c) => p.concat(`<option value="${c.bbs_address}:${c.bbs_port}">${c.bbs_name}</option>`), '')
    +'</optgroup>';
};

exports.dialButton = function (handler) {
  const btn = document.querySelector('a#dial');
  btn.addEventListener('click', handler);
};

exports.hangupButton = function (handler) {
  const btn = document.querySelector('a#hangup');
  btn.addEventListener('click', handler);
};

exports.downloadButton = function (handler) {
  const btn = document.querySelector('a#download');
  btn.addEventListener('click', handler);
}

exports.muteButton = function (button) {
  const el = document.querySelector(button);
  el.classList.add('muted');
};

exports.unmuteButton = function (button) {
  const el = document.querySelector(button);
  el.classList.remove('muted');
};
