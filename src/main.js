window.regeneratorRuntime = require("regenerator-runtime");
var c64 = require('./c64');
c64.ready = require('./term');

window.onload = function () {
  c64.run();
}
