var cpu = require('./cpu');
var images = require('./images');
var Colodore = require('./colodore-gl');

module.exports = (function() {
	var memory = cpu.memory;
	var charsUpdated=false;
	var scrollY = 0;
	var scrollYcoarse = 0;
	var scrollYfine = 0;
	var scrollBackScreen = [];
	var scrollBackColor = [];

	cpu.reset();
	cpu.trapWrites(0x0400, 0x0400+999, updateChar);
	cpu.trapWrites(0xd800, 0xd800+999, updateChar);
	cpu.trapWrites(0xd020, 0xd020, repaint);
	cpu.trapWrites(0xd021, 0xd021, repaint);
	cpu.trapWrites(0x0291, 0x0291, repaint);

	function charOut(a) {
		// Handle the cursor just like the KERNEL-editor
		cpu.memory[0xcc] = 1;
		cpu.memory[0x292] = 1;
		if (cpu.memory[0xcf] != 0) {
			cpu.memory[0xcf] = 0;
			cpu.jsr(0xea13, {a:cpu.memory[0xce], x:cpu.memory[0x287]});
		}

		// Emit character
		cpu.jsr(0xffd2, {a:a});

		// Re-enable cursor
		cpu.memory[0xcc] = 0;
		cpu.memory[0xcd] = 1; // Force cursor timeout to 1 (makes cursor visible on the next IRQ)

		// Disable qoute-mode
		cpu.memory[0xd4] = 0;

		// Clear outstanding inserts
		cpu.memory[0xd8] = 0;
	}

	function drawChar(ch, cx, cy, fgcolor) {
		ch += (memory[0x0291]&0x80) == 0x80 ? 256 : 0;
		Colodore.drawChar(ch, cx, cy, fgcolor, memory[0xd021]&15);
	}

	function snapshotFirstRow() {
		for (var x=0; x<40; x++) {
			scrollBackScreen.push(memory[0x0400+x]);
			scrollBackColor.push(memory[0xd800+x]);
		}
	}

	function snapshotScreen() {
		for (var x=0; x<40*25; x++) {
			scrollBackScreen.push(memory[0x0400+x]);
			scrollBackColor.push(memory[0xd800+x]);
		}
	}

	function renderScreen() {
		Colodore.clear(memory[0xd020]&15);
		Colodore.beginDrawChars();
		Colodore.enableBorderMask(true);
		for (var y=0; y<26; y++)
			for (var x=0; x<40; x++) {
				var sy = y+scrollYcoarse;
				if ((sy>=0) && (sy<25))
					drawChar(memory[0x0400+x+sy*40], x, y-scrollYfine, memory[0xd800+x+sy*40]&15);
				else {
					var h = Math.floor(scrollBackScreen.length/40);
					var o = h+sy;
					if ((o>=0) && (o<h))
						drawChar(scrollBackScreen[x+o*40], x, y-scrollYfine, scrollBackColor[x+o*40]&15);
				}
			}
		Colodore.enableBorderMask(false);
	}

	function updateChar(address, value) {
		if (texture!==undefined)
			return;
		address &= 0x3ff;
		var x = address%40;
		var y = Math.floor(address/40);
		var sy = y-scrollYcoarse;
		if ((sy>=0) && (sy<26)) {
			Colodore.beginDrawChars();
			Colodore.enableBorderMask(true);
			drawChar(memory[0x0400+x+y*40], x, sy-scrollYfine, memory[0xd800+x+y*40]&15);
			Colodore.enableBorderMask(false);
			charsUpdated=true;
		}
	}

	function repaint() {
		if (texture===undefined)
			renderScreen();
		else {
			Colodore.clear(11);
			Colodore.drawImage(texture);
			Colodore.drawString("Press space to continue", 9,-2, 1,11);
			Colodore.drawString(image.title, 0,26, 1,11);

			var tmp = image.author+", "+image.year;
			Colodore.drawString(tmp, 40-tmp.length,26, 1,11);
		}
		Colodore.repaint();
	}

	function setBrightness(v) {
		repaint();
	}

	function loadImage(name) {
		var img = images["vic2"][name];
		var decodedImage = atob(img.data);
		var image = new Array();
		var currentRow;
		for (var i=0; i<decodedImage.length; i++) {
			if ((i % (img.width/2)) == 0) {
				currentRow = new Array();
				image.push(currentRow);
			}

			var v = decodedImage.charCodeAt(i) & 0xff;
			var p1 = (v>>4)&15;
			var p2 = v&15;

			currentRow.push(p1);
			currentRow.push(p2);
		}
		image.title = img.title;
		image.author = img.author;
		image.year = img.year;
		return image;
	}

	var texture;
	var image;
	function run() {
		var fb = document.getElementById('framebuffer');
		Colodore.init(fb);

		var emu = document.getElementById('emulator');
		emu.style.width = emu.style.maxWidth = fb.style.width;
		emu.style.height = emu.style.maxHeight = fb.style.height;

		var scrollerContainer = document.getElementById('scrollerContainer');
		scrollerContainer.style.width = scrollerContainer.style.maxWidth = fb.style.width;
		scrollerContainer.style.height = scrollerContainer.style.maxHeight = fb.style.height;

		var scroller = document.getElementById('scroller');
		var initialHeight = parseInt(fb.style.height);
		scroller.style.width = fb.style.width;
		scroller.style.height = fb.style.height;

		scrollerContainer.addEventListener("scroll", (event) => {
			var currentHeight = parseInt(scroller.style.height)-initialHeight;
			scrollY = scrollerContainer.scrollTop-currentHeight; // 0 = normal screen, <0 = amount back
			scrollYcoarse = Math.floor(scrollY/24);
			scrollYfine = (Math.floor(scrollY/3)&7)/8;
			repaint();
		});

		// Forward some events through the scroller container down to the framebuffer
		var forwardedEvents = ["mousemove", "click", "mousedown", "mouseup", "mouseover", "mouseout"];
		var forwarder = (event) => {
			fb.dispatchEvent(new event.constructor(event.type, event));
		};
		for (var i in forwardedEvents)
			scrollerContainer.addEventListener(forwardedEvents[i], forwarder);

/*
		cpu.debug(true);
		cpu.runInstructions(600000);
		var chars = [0x50,0x52,0x49,0x4e,0x54,0x0d];
		for (var i=0; i<chars.length; i++)
			memory[0x277+i]=chars[i];
		memory[0xc6]=chars.length;
		cpu.runInstructions(10000);
		document.getElementById('debug').innerHTML = cpu.debug();
		repaint();
		*/

 
		externalInterface.ready();
	}

	var externalInterface = {
		memory:memory,
		cpu:cpu,
		repaint:repaint,
		snapshotFirstRow:snapshotFirstRow,
		snapshotScreen:snapshotScreen,
		run:run,
		ready:undefined,
		setBrightness:setBrightness,
		charOut:charOut,
		charsUpdated:function() {
			var ret = charsUpdated;
			charsUpdated = false;
			return ret;
		},
		setProperty:function(name, value) {
			Colodore.setProperty(name, value);
			if (texture!==undefined)
				texture = Colodore.convertImage(image);
			repaint();
		},
		screenSaver(enabled) {
			if (enabled===undefined)
				return image!==undefined;

			if (!enabled) {
				image = undefined;
				texture = undefined;
			} else {
				var names = [];
				for (var name in images["vic2"])
					names.push(name);
				//names = ["fire"];
				image = loadImage(names[Math.floor(Math.random()*names.length)]);
				texture = Colodore.convertImage(image);
			}
			repaint();
		}
	};

	return externalInterface;
})();
