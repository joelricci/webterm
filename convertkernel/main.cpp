#include <stdio.h>

int main(int argc, char** argv) {
    unsigned char kernel[8192];
    
    FILE *f = fopen(argv[1], "rb");
    int s = fread(kernel, 1, 8192, f);
    fclose(f);
    
    printf("module.exports = [\n");
    for (int i=0; i<s; i++) {
        if ((i&31)==0)
            printf("\t");
        printf("0x%02x", kernel[i]);
        if ((i&31)==31) {
            if (i!=s-1)
                printf(",");
            printf("\n");
        }
        else
            printf(",");
    }
    printf("];\n");
    
    return 0;
}