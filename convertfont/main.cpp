#include <stdio.h>

int main(void) {
    unsigned char font[4096];
    
    FILE *f = fopen("font.bin", "rb");
    fread(font, 1, 4096, f);
    fclose(f);
    
    for (int y=0; y<8; y++) {
        for (int x=0; x<256*2*8; x++) {
            int offset = (x&0xfff8)+y;
            int bit = 1<<(7-(x&7));
            int value = !!(font[offset]&bit);
            putchar(value*255); // istället för 00 och 01 bytes, => 00 och ff bytes
        }
    }
    
    return 0;
}